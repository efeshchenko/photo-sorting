
import os, datetime, shutil
from dateutil import parser

from PIL import Image
from PIL.ExifTags import TAGS

class Sorter():
    def __init__(self, source_dir, result_dir):
        self.source_dir = source_dir
        self.result_dir = result_dir

    def get_exif_date(self, fn):
        i = Image.open(fn)
        raw_info = i._getexif()
        decoded_info = {}
        for tag, value in raw_info.iteritems():
            decoded_key = TAGS.get(tag, tag)
            decoded_info[decoded_key] = value
                
        res = decoded_info.get("DateTimeOriginal", None)
        if res:
            res = parser.parse(res)
        return res

    def get_file_date(self, file_name):
        exif_date = self.get_exif_date(file_name)
        if exif_date:
            return exif_date
        ctime = os.path.getctime(file_name)
        file_system_date = datetime.datetime.fromtimestamp(ctime)
        return file_system_date

    def get_files(self):
        file_names = {}
        for item in os.walk(self.source_dir):
            for f in item[2]:
                if os.path.splitext(f)[1].lower() in [".jpg", ".jpeg"]:
                    file_name = os.path.join(item[0], f)
                    file_date = self.get_file_date(file_name)
                    file_names[file_name] = file_date
        return file_names
    
    def move_file(self, file_path, date):
        year = str(date.year)
        month = date.strftime("%B")
        day = str(date.day)

        path = os.path.join(self.result_dir, year, month, day)
        if not os.path.exists(path):
            os.makedirs(path)
        destination = os.path.join(path, os.path.basename(file_path))
        shutil.copy2(file_path, destination)

    def sort(self):
        if not os.path.exists(self.result_dir):
            os.mkdir(self.result_dir)
        file_names = self.get_files()
        for file_path, date in file_names.iteritems():
            self.move_file(file_path, date)
        return ''
